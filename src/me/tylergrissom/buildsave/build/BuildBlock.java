package me.tylergrissom.buildsave.build;

import org.bukkit.Material;
import org.bukkit.configuration.serialization.ConfigurationSerializable;

import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (c) 2013-2016 Tyler Grissom
 */
public class BuildBlock implements ConfigurationSerializable {

    private String materialName;
    private byte data;
    private int relativeX, relativeY, relativeZ;

    public Material getMaterial() {
        return Material.getMaterial(this.materialName);
    }

    public String getMaterialName() { return materialName; }

    public byte getData() {
        return data;
    }

    public int getRelativeX() {
        return relativeX;
    }

    public int getRelativeY() {
        return relativeY;
    }

    public int getRelativeZ() {
        return relativeZ;
    }

    public void setMaterialName(Material material) {
        this.materialName = material.toString();
    }

    public void setMaterialName(String materialName) { this.materialName = materialName; }

    public void setData(byte data) {
        this.data = data;
    }

    public void setRelativeX(int relativeX) {
        this.relativeX = relativeX;
    }

    public void setRelativeY(int relativeY) {
        this.relativeY = relativeY;
    }

    public void setRelativeZ(int relativeZ) {
        this.relativeZ = relativeZ;
    }

    public BuildBlock() {

    }

    public BuildBlock(Map<String, Object> map) {
        this.materialName = (String) map.get("materialName");
        this.data = (Byte) map.get("data");
        this.relativeX = (Integer) map.get("relativeX");
        this.relativeY = (Integer) map.get("relativeY");
        this.relativeZ = (Integer) map.get("relativeZ");
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<>();

        map.put("materialName", materialName);
        map.put("data", data);
        map.put("relativeX", relativeX);
        map.put("relativeY", relativeY);
        map.put("relativeZ", relativeZ);

        return map;
    }
}
