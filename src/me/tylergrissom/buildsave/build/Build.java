package me.tylergrissom.buildsave.build;

import org.bukkit.configuration.serialization.ConfigurationSerializable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Copyright (c) 2013-2016 Tyler Grissom
 */
public class Build implements ConfigurationSerializable {

    private String name, permission;
    private List<BuildBlock> blocks;
    private List<String> description;

    public String getName() {
        return name;
    }

    public String getPermission() {
        return permission;
    }

    public List<BuildBlock> getBlocks() {
        return blocks;
    }

    public List<String> getDescription() {
        return description;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public void setBlocks(List<BuildBlock> blocks) {
        this.blocks = blocks;
    }

    public void setDescription(List<String> description) {
        this.description = description;
    }

    public Build() {

    }

    public Build(Map<String, Object> map) {
        this.name = (String) map.get("name");
        this.permission = (String) map.get("permission");
        this.blocks = (ArrayList<BuildBlock>) map.get("blocks");
        this.description = (ArrayList<String>) map.get("description");
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<>();

        map.put("name", name);
        map.put("permission", permission);
        map.put("blocks", blocks);
        map.put("description", description);

        return map;
    }
}
