package me.tylergrissom.buildsave;

import me.tylergrissom.buildsave.build.Build;
import me.tylergrissom.buildsave.build.BuildBlock;
import me.tylergrissom.buildsave.command.CommandBase;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Copyright (c) 2013-2016 Tyler Grissom
 */
public class BuildSavePlugin extends JavaPlugin {

    private BuildSavePlugin plugin;

    public BuildSavePlugin getPlugin() {
        return plugin;
    }

    private void registerListener(Listener listener) {
        Bukkit.getPluginManager().registerEvents(listener, this);
    }

    private void registerCommand(CommandBase commandBase) {
        getCommand(commandBase.getCommandName()).setExecutor(commandBase);
    }

    @Override
    public void onEnable() {
        plugin = this;

        getConfig().options().copyDefaults(true);
        saveConfig();

        ConfigurationSerialization.registerClass(Build.class);
        ConfigurationSerialization.registerClass(BuildBlock.class);

        { // TODO DISPOSE OF THIS ONCE IMPLEMENTED!!!
            List<Build> builds = new ArrayList<>();
            Build build = new Build();
            BuildBlock buildBlock = new BuildBlock();

            buildBlock.setMaterialName(Material.DIRT);
            buildBlock.setData((byte) 0);
            buildBlock.setRelativeX(0);
            buildBlock.setRelativeY(0);
            buildBlock.setRelativeZ(0);

            build.setBlocks(Collections.singletonList(buildBlock));
            build.setDescription(Arrays.asList("This is another", "test"));
            build.setName("Another Test");
            build.setPermission(null);

            builds.add(build);

            plugin.getConfig().set("test", builds);
            plugin.saveConfig();
        }
    }

    @Override
    public void onDisable() {
        HandlerList.unregisterAll(this);
    }
}
